#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/app/BupaCommand.php';
require __DIR__.'/app/Process.php';


use Bupa\BupaCommand as Bupa;
use Symfony\Component\Console\Application;

$cmd = new Application();
$cmd->add(new Bupa());
$cmd->run();