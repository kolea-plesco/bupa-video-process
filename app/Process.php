<?php
namespace Bupa;

class Process {
	protected $output = null;
	protected $cmd = 'ffmpeg -y -nostats -loglevel 0';

	protected static $map = null;

	public function __construct(){
		
	}

	public static function getMap(){
		if (is_null(self::$map)) {
			self::$map = [];
			if (($handle = fopen(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'map.csv', "r")) !== FALSE) {
				while (($data = fgetcsv($handle)) !== FALSE) {
					self::$map[$data[0]] = $data[1];
				}
				fclose($handle);
			}
		}

		return self::$map;
	}

	public function getFileName($input, $ext, $map = true){
		$fileName = pathinfo($input, PATHINFO_FILENAME);
		return
			($map ? self::getMap()[$fileName] : $fileName).'.'.$ext;
	}

	public function concat(&$files, $dest){
		$input = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'input.txt';
		file_put_contents($input, implode('', array_map(function($f) {
			return "file '" . $f . "'" . PHP_EOL;
		}, $files)), LOCK_EX);
		$cmd = $this->cmd . ' -f concat -i ' . $input . ' -c copy -f mp4 "' . $dest . '"';
		exec($cmd);
	}

	public function trans(&$file, $dest, $params){
		$cmd = $this->cmd . ' -i "' . $file . '" ' . $params . ' "' . $dest . '"';
		exec($cmd);
	}

	/**
	 * @param $file
	 * @param $dest
	 * @param $params: ['start', 'end', 'duration']
	 */
	public function cut(&$file, $dest, $params){
		$cmd = $this->cmd . ' -i "' . $file . '" ' . $params . ' "' . $dest . '"';
		exec($cmd);
	}
}