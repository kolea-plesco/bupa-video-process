<?php
namespace Bupa;

use Bupa\Process;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

use Symfony\Component\Finder\Finder;

class BupaCommand extends Command
{
    protected $input = null;
    protected $output = null;
    protected $helper = null;
    protected $process = null;
    protected $params = [];

    protected function configure()
    {

        $this
            ->setName('bupa:process')
            ->setDescription('Process videos for Bupa Ad')
        ;
    }

    protected function askForOriginalParams(){
        $a = 'Original';
        $this->askForParams('Please enter the Main video params (start,duration)', $a, '05.0,20.0');
    }

    protected function askForCreativeParams(){
        $a = 'Creative';
        $this->askForParams('Please enter the '.$a.' processing params', $a, '-f mp4 -b:v 2048k -b:a 128k -r 25 -ar 44100 -vf "scale=640:-1"');
    }

    protected function askForYouTubeParams(){
        $a = 'YouTube';
        $this->askForParams('Please enter the '.$a.' processing params', $a, '-f mp4 -b:v 2048k -b:a 192k -r 25 -ar 44100 -vf "scale=1920:-1"');
    }

    protected function askForIOSVideoParams(){
        $a = 'iOS Video';
        $this->askForParams('Please enter the '.$a.' processing params', $a, '-f mp4 -qscale 0 -an -vf "pad=width=640:height=1280:x=0:y=0:color=black"');
    }

    protected function askForIOSAudioParams(){
        $a = 'iOS Audio';
        $this->askForParams('Please enter the '.$a.' processing params', $a, '-f mp3 -b:a 128k -ar 44100');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = & $input;
        $this->output = & $output;

        $this->helper = $this->getHelper('question');
        $this->output->writeln('');
        $question = new ChoiceQuestion(
            '<question>Please select your process actions, comma-separated [All]</question>',
            ['All', 'Original', 'Creative', 'iOS', 'YouTube', 'YouTubeUpload', 'ScoreMap'],
            0
        );
        $question->setMultiselect(true);

        $question->setErrorMessage('Action %s is invalid.');

        $action = $this->helper->ask($input, $this->output, $question);

        $this->output->writeln('<info>You have just selected: ' . implode(' > ', $action) . '</info>');

        $this->process = new Process();

        $this->output->writeln('***');
        $this->output->writeln('<info>START Process</info>');
        $this->output->writeln('***');

        foreach ($action as $a) {
            switch ($a) {
                case 'Original':
                    $this->askForOriginalParams();
                    $this->processOriginal();
                    break;
                case 'Creative':
                    $this->askForCreativeParams();
                    $this->processCreative();
                    break;
                case 'iOS':
                    $this->askForIOSVideoParams();
                    $this->askForIOSAudioParams();
                    $this->processIOS();
                    break;
                case 'YouTube':
                    $this->askForYouTubeParams();
                    $this->processYouTube();
                    break;
                case 'ScoreMap':
                    $this->processScoreMap();
                    break;
                case 'YouTubeUpload':
                    $this->processYouTubeUpload();
                    break;
                case 'All':
                default:
                    $this->askForOriginalParams();
                    $this->askForCreativeParams();
                    $this->askForIOSVideoParams();
                    $this->askForIOSAudioParams();
                    $this->processOriginal();
                    $this->processCreative();
                    $this->processIOS();
                    $this->processYouTube();
                    break;
            }
        }


        $this->output->writeln('<info>END Process!</info>');
        $this->output->writeln('***');
        $this->output->writeln('');
        $this->output->writeln('<fg=blue>Enjoy the Bupa Interactive Ad</> <info>;</info><comment>)</comment>');
        $this->output->writeln('');
    }

    protected function processOriginal(){

        $this->output->writeln('<fg=blue>START Original</>');


        $secs = $this->params['Original'];
        $secs = explode(',', $secs);
        $mainParams  = (@$secs[0] ? '-ss 00:00:'.$secs[0] : '').(@$secs[1] ? ' -t 00:00:'.$secs[1] : '').' -c copy -f mp4';

        $finder = new Finder();
        $finder->files()->in(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'main');

        $i = 1;
        foreach ($finder as $file) {
            $fileName = $this->process->getFileName($file->getRelativePathname(), 'mp4');
            $this->output->write('<fg=blue>'.str_pad($i, 2, '0', STR_PAD_LEFT).')</> <comment>Processing ' . $file->getRelativePathname() . ' to ' . $fileName . ' ... </comment>');

            $fileSource = $file->getRealpath();

            $this->process->trans($fileSource, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'main'.DIRECTORY_SEPARATOR.$fileName, $mainParams);
            $this->output->write('<info>MAIN ... </info>');

            copy($fileSource, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'own' .DIRECTORY_SEPARATOR . $fileName);

            $files = [];
            array_push($files, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'intro'.DIRECTORY_SEPARATOR.'share.mp4');
            array_push($files, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'main'.DIRECTORY_SEPARATOR.$fileName);
            array_push($files, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'outro'.DIRECTORY_SEPARATOR.'share.mp4');
            $this->process->concat($files, __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'share'.DIRECTORY_SEPARATOR . $fileName);
            $this->output->write('<info>'.strtoupper('share') . ' ... </info>');

            $this->output->writeln('<fg=blue>DONE!</>');
            $i++;
        }
        $this->output->writeln('<fg=blue>END Original</>');
        $this->output->writeln('***');
    }


    protected function askForParams($txt, $action, $defaults){
        $q = new Question('<question>'.$txt.' ['.$defaults.']</question> ', $defaults);
        $this->params[$action] = $this->helper->ask($this->input, $this->output, $q);
    }

    protected function askForOriginal(){
        $q = new Question('<question>Process Original? [Y/n]</question> ', 'Y');
        $a = $this->helper->ask($this->input, $this->output, $q);
        switch ($a) {
            case 'Y':
                $this->processOriginal();
                break;
            default:
                exit;
                break;
        }
    }

    protected function askForCreative(){
        $q = new Question('<question>Process Creative? [Y/n]</question> ', 'Y');
        $a = $this->helper->ask($this->input, $this->output, $q);
        switch ($a) {
            case 'Y':
                $this->processCreative();
                break;
            default:
                exit;
                break;
        }
    }

    protected function processCreative(){
        $this->trans('Creative', DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'creative'.DIRECTORY_SEPARATOR);
    }

    protected function processYouTube(){
        $this->trans('YouTube', DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'youtube'.DIRECTORY_SEPARATOR);
    }

    protected function processScoreMap(){
        $is = [
            'G' => 'drink',
            'V' => 'smoke',
            'D' => 'diet',
            'B' => 'exercise'
        ];

        $file = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'score.csv';
        $csv = array_map('str_getcsv', file($file));
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header

        $json = []; //GVDB
        foreach ($csv as $k => $r) {
            $v = '';
            foreach ($is as $i => $c) {
                $v .=  $i . $r[$c];
            }
            $json[$v] = $r['stars'];
        }
        $o = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'score.json';
        file_put_contents($o, json_encode($json));

        $this->output->writeln('<info>Check "' . $o . '" for result.</info>');
        $this->output->writeln('***');
    }

    protected function processYouTubeUpload(){
        $action = 'YouTube Upload';
        $this->output->writeln('<fg=blue>START '.$action.'</>');
        require('YT.php');
        $originalDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'youtube';
        $finder = new Finder();
        $finder->files()->in($originalDir);
        $i = 1;
        foreach ($finder as $file) {
            $videoPath = $file->getRealpath();
            $this->output->write('<fg=blue>'.str_pad($i, 2, '0', STR_PAD_LEFT).')</> <comment>Uploading ' . $file->getRelativePathname() . ' ... </comment>');
            $r = uploadVideo($videoPath);
            if ($r['status']) {
                $this->output->writeln('<fg=blue>DONE!</>');
            } else {
                $this->output->writeln('<error>' . $r['error'] . '</error>');
            }
            $i++;
        }
        $this->output->writeln('<fg=blue>END '.$action.'</>');
        $this->output->writeln('***');
    }

    protected function processIOS(){
        $this->trans('iOS Video', DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'ios'.DIRECTORY_SEPARATOR.'video'.DIRECTORY_SEPARATOR);
        $this->trans('iOS Audio', DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'ios'.DIRECTORY_SEPARATOR.'audio'.DIRECTORY_SEPARATOR, 'mp3');
    }

    protected function trans($action, $dir, $ext = 'mp4'){
        $finder = new Finder();
        switch ($action) {
            case 'iOS Video':
            case 'iOS Audio':
                $originalDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'creative';
                break;
            case 'YouTube':
                $originalDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'share';
                break;
            case 'Creative':
            default:
                $originalDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.'original'.DIRECTORY_SEPARATOR.'own';
                break;
        }
        $finder->files()->in($originalDir);

        while (!count($finder)) {
            switch ($action) {
                case 'iOS Video':
                case 'iOS Audio':
                    $this->output->writeln('<error>No Creative videos were found!</error>');
                    $this->askForCreative();
                    break;
                case 'YouTube':
                case 'Creative':
                default:
                    $this->output->writeln('<error>No Original videos were found!</error>');
                    $this->askForOriginal();
                    break;
            }
            $finder = new Finder();
            $finder->files()->in($originalDir);
        }

        $this->output->writeln('<fg=blue>START '.$action.'</>');

        $params = $this->params[$action];

        $i = 1;
        foreach ($finder as $file) {
            $fileName = $this->process->getFileName($file->getRelativePathname(), $ext, false);
            $this->output->write('<fg=blue>'.str_pad($i, 2, '0', STR_PAD_LEFT).')</> <comment>Processing ' . $fileName . ' ... </comment>');
            $path = $file->getRealpath();
            $this->process->trans($path, __DIR__ . $dir . $fileName, $params);
            $this->output->writeln('<fg=blue>DONE!</>');
            $i++;
        }
        $this->output->writeln('<fg=blue>END '.$action.'</>');
        $this->output->writeln('***');

    }
}