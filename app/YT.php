<?php

set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'google' .DIRECTORY_SEPARATOR.'apiclient'.DIRECTORY_SEPARATOR.'src');

require_once 'Google/Client.php';
require_once 'Google/Service/YouTube.php';
session_start();


/*
 * You can acquire an OAuth 2.0 client ID and client secret from the
 * Google Developers Console <https://console.developers.google.com/>
 * For more information about using OAuth 2.0 to access Google APIs, please see:
 * <https://developers.google.com/youtube/v3/guides/authentication>
 * Please ensure that you have enabled the YouTube Data API for your project.
 */

function uploadVideo($videoPath, $title = '', $description = '', $tags = [], $categoryId = null)
{
    $OAUTH2_CLIENT_ID = '20530354384-sugshjup7cmogls2nsk6g03dko7gmsdj.apps.googleusercontent.com';
    $OAUTH2_CLIENT_SECRET = 'X_vomKCqf7m3Y2hirexJN8Dr';

    $client = new Google_Client();
    $client->setClientId($OAUTH2_CLIENT_ID);
    $client->setClientSecret($OAUTH2_CLIENT_SECRET);
    $client->setScopes('https://www.googleapis.com/auth/youtube');

    $client->setAccessType('offline');

    // Define an object that will be used to make all API requests.
    $youtube = new Google_Service_YouTube($client);
    var_dump($youtube->videos->getRating('K-kzoE-usfQ'));
    die;

// Check to ensure that the access token was successfully acquired.
    if ($client->getAccessToken()) {
        try {

            // Create a snippet with title, description, tags and category ID
            // Create an asset resource and set its snippet metadata and type.
            // This example sets the video's title, description, keyword tags, and
            // video category.
            $snippet = new Google_Service_YouTube_VideoSnippet();
            if ($title) {
                $snippet->setTitle($title);
            }
            if ($description) {
                $snippet->setDescription($description);
            }
            if ($tags) {
                $snippet->setTags($tags);
            }

            // Numeric video category. See
            // https://developers.google.com/youtube/v3/docs/videoCategories/list
            if ($categoryId) {
                $snippet->setCategoryId($categoryId);
            }

            // Set the video's status to "public". Valid statuses are "public",
            // "private" and "unlisted".
            $status = new Google_Service_YouTube_VideoStatus();
            $status->privacyStatus = "private";

            // Associate the snippet and status objects with a new video resource.
            $video = new Google_Service_YouTube_Video();
            $video->setSnippet($snippet);
            $video->setStatus($status);

            // Specify the size of each chunk of data, in bytes. Set a higher value for
            // reliable connection as fewer chunks lead to faster uploads. Set a lower
            // value for better recovery on less reliable connections.
            //$chunkSizeBytes = 1 * 1024 * 1024;
            $chunkSizeBytes = filesize($videoPath);

            // Setting the defer flag to true tells the client to return a request which can be called
            // with ->execute(); instead of making the API call immediately.
            $client->setDefer(true);

            // Create a request for the API's videos.insert method to create and upload the video.
            $insertRequest = $youtube->videos->insert("status,snippet", $video);

            // Create a MediaFileUpload object for resumable uploads.
            $media = new Google_Http_MediaFileUpload(
                $client,
                $insertRequest,
                'video/*',
                null,
                true,
                $chunkSizeBytes
            );
            $media->setFileSize(filesize($videoPath));


            // Read the media file and upload it chunk by chunk.
            $status = false;
            $handle = fopen($videoPath, "rb");
            while (!$status && !feof($handle)) {
                $chunk = fread($handle, $chunkSizeBytes);
                $status = $media->nextChunk($chunk);
            }

            fclose($handle);

            // If you want to make other calls after the file upload, set setDefer back to false
            $client->setDefer(false);

            return ['status' => true];
        } catch (Google_Service_Exception $e) {
            return ['status' => false, 'error' => $e->getMessage()];
        } catch (Google_Exception $e) {
            return ['status' => false, 'error' => $e->getMessage()];
        }
    } else {
    }
}